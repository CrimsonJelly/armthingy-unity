﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolverIterations : MonoBehaviour
{
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        //rb.solverIterations = 2147483647;
        rb.solverIterations = 180;
    }
}
