﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorScript : MonoBehaviour
{
    public float torqueVal;
    
    public bool inverseTorque;
    public float RPM;
    public enum TorqueAxis
    {
        X,
        Y,
        Z
    };
    
    public TorqueAxis axisSelection;
    Rigidbody rb;
    Vector3 torque;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.maxAngularVelocity = Mathf.PI*RPM / 30;
        torque = Vector3.zero;
        if (inverseTorque)
        {
            reverseCoeff = -1;
        }
        else
        {
            reverseCoeff = 1;
        }

        if(axisSelection == TorqueAxis.X)
        {
            torque.x = torqueVal * reverseCoeff;
        }
        if (axisSelection == TorqueAxis.Y)
        {
            torque.y = torqueVal * reverseCoeff;
        }
        if (axisSelection == TorqueAxis.Z)
        {
            torque.z = torqueVal * reverseCoeff;
        }
        //Debug.Log(torque);
        rb.AddRelativeTorque(torque);
    }
}
